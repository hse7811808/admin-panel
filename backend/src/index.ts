import bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';
import passport from 'passport';
import cookieParser from 'cookie-parser';
import serveStatic from 'serve-static';
import expressSession from 'express-session';
import mongoose from 'mongoose';
import router from './routes';
import { enablePassport } from './auth/auth';

const app = express();

app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json(

));
app.use(cors({
    origin: 'http://localhost:3000'
}));
app.use(cookieParser());

// Middlewares, которые должны быть определены до passport:
app.use(serveStatic(__dirname + '/../public'));
app.use(expressSession({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

// passport.serializeUser(UserModel.serializeUser());
// passport.deserializeUser(UserModel.deserializeUser());

// Passport:
// app.use(passport.initialize());
// app.use(passport.session());

enablePassport();

app.use(router);

mongoose.connect('mongodb://127.0.0.1:27017/local')
    .then(
        () => console.log('connected to mongodb'),
        (err) => console.log(err)
    );

app.listen(3001, () => {
    console.log('Server started working on port 3001');
})