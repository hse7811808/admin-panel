import mongoose, { Schema } from "mongoose";

const SaladSchema = new Schema({
    ingridients: [{
        type: Schema.Types.ObjectId,
        ref: 'Ingridient',
        required: true,
    }],
    cooker: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    name: {
        type: String,
        required: true,
    }
});

export const SaladModel = mongoose.model('Salad', SaladSchema);