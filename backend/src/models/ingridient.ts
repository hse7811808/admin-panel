import mongoose, { Schema } from "mongoose";

const IngridientSchema = new Schema({
    name: { type: String, required: true },
    salad: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Salad'
        }
    ],
});

export const IngridientModel = mongoose.model('Ingridient', IngridientSchema);