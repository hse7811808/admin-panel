import { Router } from 'express';
import passport from 'passport';
import protectedRouter from "./protected";
import UserRouter from './user';

const router = Router();

router.use('/api', passport.authenticate('jwt', { session: false }), protectedRouter);
router.use('/user', UserRouter);

export default router;