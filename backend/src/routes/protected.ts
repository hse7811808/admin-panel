import { Router } from 'express';
import passport from 'passport';
import ingridientRouter from './ingridient';
import SaladRouter from './salad';

const router = Router();

router.use('/ingridients', ingridientRouter);
router.use('/salads', SaladRouter);

export default router;