import { Router } from "express";
import { SaladController } from "../controllers/salad";

const router = Router()

router.get('/', SaladController.findAll);
router.get('/:id', SaladController.findById);
router.post('/', SaladController.create);
router.put('/', SaladController.update);
router.delete('/:id', SaladController.delete);

export default router;