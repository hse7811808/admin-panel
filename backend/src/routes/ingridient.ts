import { Router } from "express";
import { IngridientController } from "../controllers/ingridient";

const router = Router()

router.get('/', IngridientController.findAll);
router.get('/:id', IngridientController.findById);
router.post('/', IngridientController.create);
router.put('/', IngridientController.update);
router.delete('/:id', IngridientController.delete);

export default router;