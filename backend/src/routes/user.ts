import { Router } from "express";
import passport from "passport";
import UserController from "../controllers/user";

const router = Router()

router.post('/register', passport.authenticate('signup', { session: false }), UserController.register);
router.post('/login', UserController.login);

export default router;