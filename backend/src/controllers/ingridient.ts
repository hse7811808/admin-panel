import { RequestHandler } from "express";
import { IngridientModel } from "../models/ingridient";

export const IngridientController: Record<string, RequestHandler> = {
    findAll: async (req, res) => {
        console.log('controller', req.headers)
        const data = await IngridientModel.find({}).populate('salads').exec();

        res.json({ items: data });
    },
    findById: async (req, res) => {
        const id = req.params.id;
        const data = await IngridientModel.findOne({ _id: id }).populate('salads').exec();

        res.json({ data });
    },
    create: async (req, res) => {
        const data = req.body;

        IngridientModel.create(data)
            .then(
                () => res.sendStatus(200),
                (err) => res.status(401).json({ error: err })
            );
    },
    delete: async (req, res) => {
        const id = req.body;

        IngridientModel.deleteOne({ _id: id }).exec()
            .then(
                () => res.sendStatus(200),
                (err) => res.status(401).json({ error: err })
            );
    },
    update: async (req, res) => {
        const data = req.body;

        IngridientModel.findByIdAndUpdate(data.id, data).exec()
            .then(
                () => res.sendStatus(200),
                (err) => res.status(401).json({ error: err }),
            )
    }
}