import { RequestHandler } from "express";
import mongoose from "mongoose";
import { IngridientModel } from "../models/ingridient";
import { SaladModel } from "../models/salad";
import { UserModel } from "../models/user";
import { IngridientController } from "./ingridient";

export const SaladController: Record<string, RequestHandler> = {
    findAll: async (req, res) => {
        const data = await SaladModel
            .find({})
            .populate({ path: 'ingridients', select: ['_id', 'name'] })
            .populate({ path: 'cooker', select: ["_id", "username"] })
            .exec();

        res.json({ items: data });
    },
    findById: async (req, res) => {
        const id = req.params.id;
        const data = await SaladModel
            .findById(id)
            .populate('ingridients')
            .populate({ path: 'cooker', select: ["_id", "username"] })
            .exec();

        res.json({ data });
    },
    create: async (req, res) => {
        const data = req.body;
        const ingridientsId = data.ingridients;

        const id = new mongoose.Types.ObjectId();

        try {
            ingridientsId.map(async (iid: string) => {
                const ingridient = await IngridientModel.findById(iid).exec();

                if (!ingridient) {
                    throw new Error(`Ingridient ${iid} not found`)
                }
                ingridient.salad = [...new Set(ingridient.salad), id]
                await IngridientModel.findByIdAndUpdate(iid, ingridient)

            });

            SaladModel.create({ ...data, _id: id })
                .then(
                    () => res.sendStatus(200),
                    (err) => res.status(401).json({ error: err })
                );
        } catch (err: any) {
            res.status(404).json({ error: err.message })
        }
    },
    delete: async (req, res) => {
        const id = req.body;

        SaladModel.deleteOne({ _id: id }).exec()
            .then(
                () => res.sendStatus(200),
                (err) => res.status(401).json({ error: err })
            );
    },
    update: async (req, res) => {
        const data = req.body;

        SaladModel.findByIdAndUpdate(data.id, data).exec()
            .then(
                () => res.sendStatus(200),
                (err) => res.status(401).json({ error: err }),
            )
    }
}