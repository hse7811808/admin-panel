import { RequestHandler } from "express";
import { login, register } from "../tools/login";

const UserController: Record<string, RequestHandler> = {
    register: register,
    login: login,
}

export default UserController;