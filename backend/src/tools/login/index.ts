import { RequestHandler } from "express";
import jwt from 'jsonwebtoken';
import passport from 'passport';

export const login: RequestHandler = async (req, res, next) => {
    passport.authenticate('login', async (err: any, user: any, info: any) => {
        try {
            if (err || !user) {
                const error = new Error('An Error occured')
                return next(error);
            }
            req.login(user, { session: false }, async (error) => {
                if (error) return next(error)
                //We don't want to store the sensitive information such as the
                //user password in the token so we pick only the email and id
                const body = { _id: user._id, username: user.username };
                //Sign the JWT token and populate the payload with the user email and id
                const token = jwt.sign({ user: body }, 'keyboard cat');
                //Send back the token to the user
                return res.json({ token });
            });
        } catch (error) {
            return next(error);
        }
    })(req, res, next);
};

export const register: RequestHandler = async (req, res) => {
    res.json({
        message: 'Signup successful',
        user: req.user
    });
};