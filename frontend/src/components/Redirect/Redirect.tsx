import React from 'react';
import { useNavigate } from 'react-router-dom';

interface RedirectProps {
    to: string;
    replace?: boolean;
}

const Redirect: React.FC<RedirectProps> = ({to, replace}) => {
    const navigate = useNavigate();

    React.useEffect(() => {
        navigate(to, { replace });
    }, []);

    return null;
}

export default Redirect;