import { Container, Stack, TextField, Button, CircularProgress, Typography } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'store';
import { selectLoginState } from 'store/slices/login';
import { loginThunk } from 'store/thunks/login';
import { LoadingState } from 'tools/loadingState';
import logo from './logo512.png';

const Login = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const loginState = useSelector(selectLoginState);

    React.useEffect(() => {
        if (loginState === LoadingState.SUCCESS) {
            navigate('/main');
        }
    })

    const [state, setState] = React.useState({
        username: '',
        password: '',
    });

    const onClick = React.useCallback(() => {
        dispatch(loginThunk({ ...state }));
    }, [dispatch, state]);

    const onPressEnter: React.KeyboardEventHandler = (e) => {
        if (e.code === 'enter') {
            onClick();
        }
    }

    const onChangeGen = (key: keyof typeof state) => {
        const onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
            setState({ ...state, [key]: e.target.value });
        };

        return onChange;
    }
    
    return (
        <Container sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <Stack spacing={1}>
                <img src={logo} alt='' />
                <TextField
                    onChange={onChangeGen('username')}
                    value={state.username}
                    placeholder='username'
                    label='username'
                    onKeyDown={onPressEnter}
                />
                <TextField
                    type='password'
                    onChange={onChangeGen('password')}
                    value={state.password}
                    placeholder='password'
                    label='password'
                    onKeyDown={onPressEnter}
                />
                <Typography variant='body1' color='error'>
                    {loginState === LoadingState.ERROR ? "Incorrect username or password" : null}
                </Typography>
                <Button
                    variant='contained'
                    color='primary'
                    onClick={onClick}
                    disabled={loginState === LoadingState.LOADING}
                >
                    {loginState === LoadingState.LOADING ? <CircularProgress /> : "Login"}
                </Button>
            </Stack>
        </Container>
    );
}

export default Login;