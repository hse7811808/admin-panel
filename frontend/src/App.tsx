import React from 'react';
import './App.css';
import Login from 'pages/Login/Login';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Redirect from 'components/Redirect/Redirect';
import Main from 'Main/Main';

function App() {
  const token = localStorage.getItem('token');

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Redirect to={!token? '/login' : '/main'} replace/>} />
        <Route path='/login' element={<Login />} />
        <Route path='/main' element={<Main />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
