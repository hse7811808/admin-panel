export type Salad = {
    cooker: string;
    ingridients: string[];
}