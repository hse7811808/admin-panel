export type Ingridient = {
    name: string;
    salad?: string[];
}