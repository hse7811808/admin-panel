import axios from "axios";

export const api = axios.create({
    baseURL: 'http://localhost:3001',
});

api.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
});

api.interceptors.response.use((response) => response, (err) => {
    if (err.response.status === 401) {
        window.location.href = '/login';
    }
})