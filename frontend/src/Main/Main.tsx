import React from 'react';
import { useDispatch } from 'store';
import { fetchAllIgridients } from 'store/thunks/ingridient';

const Main = () => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(fetchAllIgridients());
    }, [])

    return (
        <>kek</>
    );
}

export default Main;