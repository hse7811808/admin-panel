import { createSlice } from '@reduxjs/toolkit';
import { fetchAllIgridients } from 'store/thunks/ingridient';
import { LoadingState } from '../../tools/loadingState';
import { Ingridient } from '../../types/ingridient';

const INITIAL_STATE = {
    model: {
        state: LoadingState.IDLE,
    },
    data: {} as Record<string, Ingridient>,
}

export const ingridientsSlice = createSlice({
    name: 'ingridientsSlice',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllIgridients.pending, (state) => {
                state.model.state = LoadingState.LOADING;
            })
            .addCase(fetchAllIgridients.fulfilled, (state, data) => {
                state.model.state = LoadingState.SUCCESS;

                console.log({ data })
            })
    }
});