import { createSlice } from '@reduxjs/toolkit';
import { RootState } from 'store';
import { loginThunk } from 'store/thunks/login';
import { LoadingState } from 'tools/loadingState';

type LoginSliceStateType = {
    state: LoadingState;
}

const INITIAL_STATE: LoginSliceStateType = {
    state: LoadingState.IDLE
}

export const loginSlice = createSlice({
    name: 'loginSlice',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(loginThunk.fulfilled, (state, action) => {
                localStorage.setItem('token', action.payload.data.token);
                state.state = LoadingState.SUCCESS;
            })
            .addCase(loginThunk.rejected, (state, error) => {
                state.state = LoadingState.ERROR;
            })
            .addCase(loginThunk.pending, (state) => {
                state.state = LoadingState.LOADING;
            })
    }
});

export const selectLoginState = (state: RootState) => state.login.state;
