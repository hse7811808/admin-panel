import { createAsyncThunk } from "@reduxjs/toolkit";
import { AxiosResponse } from "axios";
import { api } from "tools/fetchAPI";

interface LoginThunkProps {
    username: string;
    password: string;
}

export const loginThunk = createAsyncThunk(
    'login',
    async ({ username, password }: LoginThunkProps) => {
        const data: AxiosResponse<{ token: string }> = await api.post('/user/login', { username, password });

        return data;
    }
)