import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "tools/fetchAPI";

export const fetchAllIgridients = createAsyncThunk(
    'ingridients/fetchAll',
    async () => {
        const { data } = await api.get('/api/ingridients', {
            headers: {
                Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjYzZmE2OGIyOTI4MzYzZDlkYzQ4NDkzYiIsInVzZXJuYW1lIjoidXNlcjMifSwiaWF0IjoxNjc3MzU1MTkyfQ.uG1-EEnM50wJ3MdyK_rZTbujY8YtALOx9UkWrsV2Xro'
            }
        });

        return data;
    }
)