import { PayloadAction } from "@reduxjs/toolkit";
import { Ingridient } from "../../types/ingridient";

export type SetIngridientsData = {
    data: Record<string, Ingridient>;
};
export type SetIngridientsDataPyaloadAction = PayloadAction<SetIngridientsData>;